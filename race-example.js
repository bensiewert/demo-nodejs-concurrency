const randomDelay = () => new Promise(resolve =>
    setTimeout(resolve, Math.random() * 100)
  )
  
  let balance = 0
  
  async function loadBalance () {
    await randomDelay()
    return balance
  }
  
  async function saveBalance (value) {
    await randomDelay()
    balance = value
  }
  
  async function sell () {
    const balance = await loadBalance()
    console.log(`sell - balance loaded: ${balance}`)
    const newBalance = balance + 50
    await saveBalance(newBalance)
    console.log(`sell - balance updated: ${newBalance}`)
  }
  
  async function main () {
    const transaction1 = sell() // NOTE: no `await`
    const transaction2 = sell() // NOTE: no `await`
    await transaction1
    await transaction2
    const balance = await loadBalance()
    console.log(`Final balance: ${balance}`)
  }
  
  main()

  