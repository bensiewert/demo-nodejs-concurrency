# Demo - Node.js Concurrency

A couple of demonstrations using npm's ```async-mutex``` library found [here](https://www.npmjs.com/package/async-mutex)

Although Node.js is single-threaded, it is still possible to experience race-conditions with asynchronous programming.

## Mutex

Wikipedia - "In computer science, a lock or mutex (from mutual exclusion) is a synchronization primitive: a mechanism that enforces limits on access to a resource when there are many threads of execution. A lock is designed to enforce a mutual exclusion concurrency control policy, and with a variety of possible methods there exists multiple unique implementations for different applications."

In short, using a mutex will control the access to a sequence of code by locking it on first-come, first serve basis. The next request will wait for the first one and when it is unlocked, then the next thread/request will do its work against the mutex controlled resource.

### run example mutex and example race condition

1. Install Node.js dependencies with ```npm install```
2. Run ```node race-example.js``` a few times and notice the account balance is off for the same operation because of the asynchronous code accessing and updating the account balance out of synchronization. 

    ![example race condition](/images/race-example.png)

3. Run ```node race-fix-mutex.js``` to see that with the Mutex implementation, we no longer have an inaccurate account balance.

## Semaphore

Todo next ;)