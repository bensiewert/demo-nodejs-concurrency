import { Mutex, Semaphore, withTimeout } from 'async-mutex'
import {Observable, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';

const mutex = new Mutex()

let subData = 0

async function printClock(val, data) {
    // await mutex.runExclusive(async() => {
    //     ticktock = tick === 'tick' ? 0 : 1
    //     console.log(`clock: ${Date.now()}`, val, ticktock)
    // })
    subData = data
    console.log(`clock: ${Date.now()}`, val, subData)
}

const clock$ = new Observable((subscriber) => {
    const interval = setInterval(() => {
        setTimeout(() => subscriber.next('tick'), Math.random())
    }, 1000);
    setTimeout(() => clearInterval(interval), 7 * 1000);
})


 const subscriptions = new Subscription()

 for (let i = 1; i < 15; i++) {
    subscriptions.add(
        clock$.pipe(
            map((val, index) => {
                return Math.random().toString()
            })
         ).subscribe(val => printClock(`sub ${i}`, val))
    )
 }

 setTimeout(() => subscriptions.unsubscribe(), 10 * 1000)
//  const subscription = clock$.pipe(
//     map((val, index) => {
//         return index % 2 === 0 ? val : 'tock'
//     })
//  ).subscribe(val => printClock(`sub 1, ${val}`));

//  const subscription2 = clock$.pipe(
//     map((val, index) => {
//         return index % 2 === 0 ? val : 'tock'
//     })
//  ).subscribe(val => printClock(`sub 2, ${val}`));
  
//  setTimeout(() => subscription.unsubscribe(), 10 * 1000);
//  setTimeout(() => subscription2.unsubscribe(), 10 * 1000);



// async function sleep(time) {
//     return new Promise((resolve) => {
//         setTimeout(resolve.bind(null), time);
//       });    
// }

// async function someAsnycOp(desc, timeout) {
    
//     return mutex.runExclusive(async () => {
//         await sleep(timeout)
//         console.log('runexclusive: ', desc)
//         return desc 
//     })
    
// }

// async function run() {
//     const ops = []
//     for (let i = 1; i < 10; i++) {
//         const timeout = Math.random() * 1000
//         ops.push(someAsnycOp(`op:${i}, timeout: ${timeout}`, timeout))
//     }
//     const result = await Promise.all(ops)
//     console.log('result: ', result)
// }

// // run()
